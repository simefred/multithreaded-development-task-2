#include <pthread.h>
#include <iostream>
#include <string>

typedef struct {
  pthread_mutex_t number_1_mutex,number_2_mutex;
  pthread_cond_t number_1_cond, number_2_cond;
  bool quit;
  int number_1, number_2;
  

} _params;

void* keyboardhandler(void * ptr);
void* multiplication_function(void* ptr);
void* output_function(void *ptr);

int main(int argc, char *argv[]) {
  _params params;
  pthread_t keyboard_thread, thread_2, thread_3;
  int rc;

  params.number_1_mutex = PTHREAD_MUTEX_INITIALIZER;
  params.number_2_mutex = PTHREAD_MUTEX_INITIALIZER;
  params.number_1_cond = PTHREAD_COND_INITIALIZER;
  params.number_2_cond = PTHREAD_COND_INITIALIZER;
  params.quit = false;
  params.number_1 = 0;
  params.number_2 = 0;

  rc = pthread_create(&keyboard_thread, NULL, keyboardhandler, &params);
  if(rc) {
    printf("ERROR; return code from ptrhead:create() is %d\n", rc);
    exit(-1);
  }
  rc = pthread_create(&thread_2, NULL, multiplication_function, &params);
  if(rc) {
    printf("ERROR; return code from ptrhead:create() is %d\n", rc);
    exit(-1);
  }
  rc = pthread_create(&thread_3, NULL, output_function, &params);
  if(rc) {
    printf("ERROR; return code from ptrhead:create() is %d\n", rc);
    exit(-1);  
  }

  //join the threads when they exit
  rc = pthread_join(keyboard_thread, NULL);
  if(rc) {
    printf("ERROR; return code from pthread_join() is %d\n", rc);
    exit(-1);
  }
  rc = pthread_join(thread_2, NULL);
  if(rc) {
    printf("ERROR; return code from pthread_join() is %d\n", rc);
    exit(-1);
  }
  rc = pthread_join(thread_3, NULL);
  if(rc) {
    printf("ERROR; return code from pthread_join() is %d\n", rc);
    exit(-1);
  }

  printf("Press ENTER to exit\n");
  std::getchar();

  return 0;
}
void* keyboardhandler(void * ptr) {
  char temp_string[256];
  std::string string;
  _params *params = (_params *)ptr;
  printf("Input a non 0 number to proceed, enter q to terminate\n");
  while(!params->quit) {
     gets_s(temp_string);
     string=temp_string;
     pthread_mutex_lock(&params->number_1_mutex);
     if(string != "0" && string !="") {   //the and is to avoid crashing with std::stoi 
          if(string=="q" || string=="Q") {
          params->quit = true;
          }
          else {             
           params->number_1 = std::stoi(string);
           printf("Keyboardhandler sending: %d\n",params->number_1);
          }
       pthread_cond_signal(&params->number_1_cond);
       
     }
     pthread_mutex_unlock(&params->number_1_mutex);
    
  }
  printf("Keyboard Handler quitting\n");
  pthread_exit(NULL);
  return 0;
}
void* multiplication_function(void* ptr) {
  _params *params = (_params *)ptr;
  while(!params->quit) {       
    pthread_mutex_lock(&params->number_1_mutex);
    pthread_cond_wait(&params->number_1_cond, &params->number_1_mutex);
    params->number_2 = params->number_1 * 3;
    printf("Worker Function turning %d",params->number_1);
    params->number_1 = 0;
    printf(" into %d \n", params->number_2);
    pthread_mutex_unlock(&params->number_1_mutex);
    pthread_mutex_lock(&params->number_2_mutex);
    pthread_cond_signal(&params->number_2_cond);
    pthread_mutex_unlock(&params->number_2_mutex);
    

    
  }
  printf("Worker Function quitting\n");
  pthread_exit(NULL);
  return 0;

}

void* output_function(void *ptr){
  _params *params = (_params *) ptr;
  while(!params->quit) {
      pthread_mutex_lock(&params->number_2_mutex);
      pthread_cond_wait(&params->number_2_cond, &params->number_2_mutex);
      printf("Output Function printing %d\n",params->number_2);
      params->number_2 = 0;
      pthread_mutex_unlock(&params->number_2_mutex);
    
  }
  printf("Output Function quitting\n");
  pthread_exit(NULL);
  return 0;
}

