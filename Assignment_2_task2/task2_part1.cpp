#include <pthread.h>
#include <iostream>
#include <string>

typedef struct {
  pthread_mutex_t number_1_mutex, number_2_mutex, number_3_mutex, bool_mutex;
  bool quit;
  int number_1, number_2;
  

} _params;


void* keyboardhandler(void * ptr);
void* multiplication_function(void* ptr);
void* output_function(void *ptr);
int main(int argc, char *argv[]) {
  _params params;
  pthread_t keyboard_thread, thread_2, thread_3;
  int rc;
   //initialize struct
  params.number_1_mutex = PTHREAD_MUTEX_INITIALIZER;
  params.number_2_mutex = PTHREAD_MUTEX_INITIALIZER;
  params.bool_mutex = PTHREAD_MUTEX_INITIALIZER;
  params.quit = false;
  params.number_1 = 0;
  params.number_2 = 0;
  //create the threads
  rc = pthread_create(&keyboard_thread, NULL, keyboardhandler, &params);
  if(rc) {
    printf("ERROR; return code from ptrhead:create() is %d\n", rc);
    exit(-1);
  }
  rc = pthread_create(&thread_2, NULL, multiplication_function, &params);
  if(rc) {
    printf("ERROR; return code from ptrhead:create() is %d\n", rc);
    exit(-1);
  }
  rc = pthread_create(&thread_3, NULL, output_function, &params);
  if(rc) {
    printf("ERROR; return code from ptrhead:create() is %d\n", rc);
    exit(-1);  
  }

  //join the threads when they exit
  rc = pthread_join(keyboard_thread, NULL);
  if(rc) {
    printf("ERROR; return code from pthread_join() is %d\n", rc);
    exit(-1);
  }
  rc = pthread_join(thread_2, NULL);
  if(rc) {
    printf("ERROR; return code from pthread_join() is %d\n", rc);
    exit(-1);
  }
  rc = pthread_join(thread_3, NULL);
  if(rc) {
    printf("ERROR; return code from pthread_join() is %d\n", rc);
    exit(-1);
  }

  printf("Press ENTER to exit\n");
  std::getchar();
  return 0;
}
void* keyboardhandler(void * ptr) {
  char temp_string[256];
  std::string string;
  _params *params = (_params *)ptr;
  printf("Input a non 0 number to proceed, enter q to terminate\n");
  while(!params->quit) {
     gets_s(temp_string);
     string=temp_string;
     if(string != "0") {
          if(string=="q" || string=="Q") {
          pthread_mutex_lock(&params->bool_mutex);
          params->quit = true;
          pthread_mutex_unlock(&params->bool_mutex);
          }
          else {
           pthread_mutex_lock(&params->number_1_mutex);              
           params->number_1 = std::stoi(string);
           printf("Keyboardhandler sending: %d\n",params->number_1);
           pthread_mutex_unlock(&params->number_1_mutex);
          }
     }
    
  }
  printf("Keyboard Handler quitting\n");
  pthread_exit(NULL);
  return 0;
}
void* multiplication_function(void* ptr) {
  _params *params = (_params *)ptr;
  while(!params->quit) {
    
    pthread_mutex_lock(&params->number_1_mutex);
    if(params->number_1 != 0) {
      pthread_mutex_lock(&params->number_2_mutex);
      params->number_2 = params->number_1 * 3;
      printf("Worker Function turning %d",params->number_1);
      params->number_1 = 0;
      printf(" into %d \n", params->number_2);
      pthread_mutex_unlock(&params->number_2_mutex);
    }
    pthread_mutex_unlock(&params->number_1_mutex);
  }
  printf("Worker Function quitting\n");
  pthread_exit(NULL);
  return 0;

}

void* output_function(void *ptr){
  _params *params = (_params *) ptr;
  while(!params->quit) {
    pthread_mutex_lock(&params->number_2_mutex);
    if(params->number_2 != 0) {
      printf("Output Function printing %d\n",params->number_2);
      params->number_2 = 0;
    }
    pthread_mutex_unlock(&params->number_2_mutex);

  }
  printf("Output Function quitting\n");
  pthread_exit(NULL);
  return 0;
}